# Rebeca García Mencía
# !/usr/bin/python3
"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar

if len(sys.argv) != 6:
    sys.exit("Usage: client.py ip puerto REGISTER sip_address expires")

SERVER = sys.argv[1]
PORT = int(sys.argv[2])
REGISTER = sys.argv[3]
ACCOUNT = sys.argv[4]
EXPIRES = sys.argv[5]

if str(sys.argv[3]) != "REGISTER":
    sys.exit("Debe utilizarse REGISTER")

if int(EXPIRES) < 0:
    sys.exit("El valor de expires tiene que ser positivo")
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    MESSAGE = REGISTER + " " + "sip:" + ACCOUNT + " SIP/2.0\r\n"
    MESSAGE += "Expires: " + str(EXPIRES) + "\r\n\r\n"
    print("Enviando: ", MESSAGE)
    my_socket.send(bytes(MESSAGE, 'utf-8'))
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
