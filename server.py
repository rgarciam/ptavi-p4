# Rebeca García Mencía
# !/usr/bin/python3
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import json
import socketserver
import sys
from time import gmtime, localtime, strftime, time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    IP_dicc = {}

    def register2json(self):
        """Creación de fichero json."""
        fichero_json = "registered.json"

        with open(fichero_json, 'w') as fichero:
            json.dump(self.IP_dicc, fichero, indent=4)

    def json2registered(self):
        """Comprobar si hay fichero registered.json."""
        try:
            with open("registered.json", 'r') as fichero:
                self.dicc_IP = json.load(fichero)
        except FileNotFoundError:
            pass

    def handle(self):
        """
        Handle method of the server class.
        (all requests will be handled by this method).
        """
        line = self.rfile.read()
        decodificar = line.decode('utf-8')
        print("El cliente nos manda ", decodificar)
        separador1 = decodificar.split()
        metodo = separador1[0]
        if metodo != "REGISTER":
            sys.exit("SIP/2.0 400 Solicitud Erronea\r\n")
        user = separador1[1].split(":")[1]
        expires = int(separador1[-1])
        IP = self.client_address[0]
        if expires == 0:

            if user in self.IP_dicc:

                del self.IP_dicc[user]
        else:

            expired = expires + time()
            end_time = strftime('%Y-%m-%d %H:%M:%S', localtime(expired))
            self.IP_dicc[user] = ["address: " + IP, "expires: " + end_time]
            lista = []
            tiempo_actual = strftime('%Y-%m-%d %H:%M:%S', localtime(time()))

            for name in self.IP_dicc:

                if ("expires: " + tiempo_actual) >= self.IP_dicc[name][1]:
                    lista.append(name)

            for name in lista:
                del self.IP_dicc[name]

        self.wfile.write(b"SIP/2.0 200 OK\r\n")
        print(self.IP_dicc)

        self.register2json()
        self.json2registered()


if __name__ == "__main__":
    """Listens at localhost ('') port introduced by user
       and calls the EchoHandler class to manage the request."""
    if len(sys.argv) != 2:
        sys.exit("Usage: server.py puerto")
    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
